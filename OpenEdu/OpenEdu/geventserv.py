from socketio.server import SocketIOServer
import sys

from gevent import monkey
monkey.patch_all()

import WebRTC


if __name__ == '__main__':

        ip='localhost'
        if sys.argv[1]:
            ip = sys.argv[1]

        try:
            SocketIOServer((ip,5462),WebRTC.Controller(),transports=['websocket']).serve_forever()
        except RuntimeError:
            print 'The entered Ip is invalid'

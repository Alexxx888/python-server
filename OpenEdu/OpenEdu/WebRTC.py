import os
os.environ["DJANGO_SETTINGS_MODULE"]='OpenEdu.OpenEdu.settings'
print os.environ["DJANGO_SETTINGS_MODULE"]
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from OpenEdu.OpenEdu.apps.main_site import models
from socketio.mixins import BroadcastMixin
from socketio.namespace import *
from socketio import socketio_manage
from socketio.mixins import RoomsMixin
from OpenEdu.OpenEdu.apps.main_site.views import get_active_user_courses
import json




class Controller(object):
    def __init__(self):
        self.buffer = []

        self.request = {
            'nicknames': [],
        }

    def __call__(self, environ, start_response):

        path = environ['PATH_INFO'].strip('/')

        if path.startswith("socket.io"):
            socketio_manage(environ,
                            {
                                '/rtc': WebRtcSpace,
                                '/chat': Chat,

                            }, self.request)
        else:
            return self.not_found(start_response)

    def not_found(self, start_response):
        start_response('404 Not Found', [])
        return ['<h1>Not Found</h1>']




class RoomUser(object):

    def __init__(self,rnd,name,db_id,socket_id,room):
        self.rnd_name= rnd
        self.name=name
        self.socket_id=socket_id
        self.db_id=db_id
        self.room = room

    def __eq__(self, other):
        return self.rnd_name==other.rnd_name






class WebRtcSpace(BaseNamespace, BroadcastMixin,RoomsMixin):
    userList = {}

    alist = {}

    user_list=[]
    ### populated with RoomUser objects

    def find_roomuser_by_rndname(self,rnd):
        for item in self.user_list:
            if item.rnd_name == unicode(rnd):
                return item

        return

    def find_roomuser_by_dbname(self,name):
        for item in self.user_list:
            if item.name==name:
                return item
        return

    def on_userInitiatedLocalStream(self,data):

        #print data
        user = self.find_roomuser_by_dbname(data['name'])
        if user is None:
            user = self.find_roomuser_by_rndname(data['name'])
        #print self.room
        print user
        data = dict(username=user.name,name=user.rnd_name)

        self.emit_to_room(self.room,'user_is_ready',data)
        self.broadcast_to_socket(self.get_socketid(),'user_is_ready',data)
        #self.broadcast_to_socket(self.get_socketid(),'user_is_ready',username)



############ broadcast event directly to socket #####################
    def broadcast_to_socket(self, session_id, event, *args):
        pkt = dict(type='event',
                   name=event,
                   args=args,
                   endpoint=self.ns_name)

        returnValue = False

        try:
            for sessid, socket in self.socket.server.sockets.iteritems():
                if unicode(session_id) == unicode(sessid):
                    socket.send_packet(pkt)
                    returnValue = True
                    break
        except:
        #app.logger.exception('')
            return returnValue

    def room_user_list(self):
        new_list ={}
        for item in self.user_list:
            if item.room== self.room:
                new_list[item.rnd_name]=item.name
        return new_list





    def on_sendLocalDspOrIce(self, data):
        target = self.alist[data['targetUser']]
        print data
        self.broadcast_to_socket(target, 'gotLocalDSP', data)

    def on_getAllUsers(self,data):
        room_name  = data['room']
        self.emit_to_room(self.room,'returnAllUsers', self.room_user_list());
        self.broadcast_to_socket(self.get_socketid(),'returnAllUsers', self.room_user_list())



    def recv_disconnect(self):
        try:
            name =  self.socket.session['name']
            print 'removing '+name
            print self.userList
            print self.user_list

            del self.userList[name]
            del self.user_list[self.user_list.index(self.find_roomuser_by_rndname(name))]

            self.emit_to_room(self.room, 'returnAllUsers' , self.room_user_list());
            self.broadcast_to_socket(self.get_socketid(), 'returnAllUsers' , self.room_user_list());
            self.disconnect(silent=True)

        except Exception as e:
            print e


    def get_sessionid_from_environ(self):
        cookies = self.environ['HTTP_COOKIE'].split(';')
        sessionid=''
        print self.environ
        for item in cookies:
            if "sessionid=" in item:
                sessionid=item.split('=')[1]


        #print sessionid
        return sessionid

    def get_user_by_sid(self,sid):

        session_db_object =Session.objects.get(session_key=sid)
        user_db_id = session_db_object.get_decoded().get('_auth_user_id')
        user_db = User.objects.get(pk=user_db_id)

        user_courses = models.SessionModel.objects.filter(participants=user_db)
        #print user_courses

    def get_socketid(self):
        s= self.environ['PATH_INFO']
        s = s[self.environ['PATH_INFO'].rfind('/') + 1:]
        return s





    def on_registerUser(self, data):

        self.room = data['room']
        self.join(data['room'])
        sessionid=self.get_sessionid_from_environ()


        if sessionid=="":
            print data
            self.user_list.append(RoomUser(data['user'],data['user'],-1,self.get_socketid(),self.room))
            self.alist[data['user']] = self.get_socketid()
            self.socket.session['name'] = data['user']
            self.userList[data['user']]=data['user']
            self.socket.session['username'] = data['user']


        else:

            session_db_object =Session.objects.get(session_key=sessionid)
            user_db_id = session_db_object.get_decoded().get('_auth_user_id')
            user_db = User.objects.get(pk=user_db_id)

            user_courses = models.SessionModel.objects.filter(participants=user_db)

            self.socket.session['username'] = user_db.username
            self.socket.session['name'] = data['user']


            socket = self.get_socketid()
            self.alist[data['user']] = socket
            #print socket
            self.userList[data['user']]=user_db.username


            self.user_list.append(RoomUser(data['user'],user_db.username,user_db.id,socket,self.room))
       # print 'session test' + self.session['name']

    def on_call(self, data):

        user = self.find_roomuser_by_rndname(data['targetUser'])
        self.broadcast_to_socket(user.socket_id, 'receivedCall', data)

    def on_callAction(self, data):
        print data
        target = self.alist[data['targetUser']]
        target2= self.find_roomuser_by_rndname(data['targetUser'])
        print target2
        #try:
        self.broadcast_to_socket(target2.socket_id, 'callAction', data)
        #except AttributeError:
        #    pass


    def on_renegotiation(self,data):
        print 'negotiation'
        print data

        try:
           socket = self.find_roomuser_by_rndname(data['target']).socket_id
        except:
            print 'error in on_renegotiaton'


        self.broadcast_to_socket(socket,"renegotiation",data)

    def on_removeDone(self,data):
        try:
            print "on remove" + data['target']
            self.broadcast_to_socket(self.find_roomuser_by_rndname(data['target']).socket_id,"removeDone",data)
        except KeyError:
            print 'error in removedone'






#inherits the webrtc class
class Chat(WebRtcSpace):

    #webrtcspace is also initiated for this socket meaning the session has 'name':'name' argument
    def on_chat_room_created(self,data):




        if data is not None:

            try:
                rnd_name= unicode(data['sender'])

                user_db_id = self.find_roomuser_by_rndname(rnd_name).db_id
                print user_db_id
                if models.SessionModel.objects.filter(unique_key = data['room']).exists() and user_db_id<0:
                    self.broadcast_to_socket(self.get_socketid(),'receivedMessage',{'data':"You don`t have permission to access this channel",'sender':"System","room":"","username":"System"})
                    return
                else:


                    if models.SessionModel.objects.filter(unique_key=data['room']).exists():
                        if models.SessionModel.objects.filter(unique_key=data['room'],participants__id=user_db_id).exists():
                            self.room=data['room']
                            self.join(data['room'])
                            self.socket.session['class_room']=data['room']
                            self.broadcast_to_socket(self.get_socketid(),'receivedMessage',{'data':"Channel joined successfully",'sender':"System","room":"","username":"System"})
                            return


                    else:
                       # self.broadcast_to_socket(self.get_socketid(),'receivedMessage',{'data':"You don`t have permission to access this channel",'sender':"System","room":""})
                        return









            except Exception as e:
                print e


            self.room=data['room']
            self.join(data['room'])

            self.broadcast_to_socket(self.get_socketid(),'receivedMessage',{'data':"Channel joined successfully",'sender':"System","room":"","username":"System"})
    def on_sendMessage(self, data):


            data['username']= self.session['username']

             #print self.environ
            # print self.request
            #check if user in room

            if data['room'] == self.room:
                self.emit_to_room(self.room,'receivedMessage',data)
                self.broadcast_to_socket(self.get_socketid(),'receivedMessage',data)

            else:
                self.disconnect()





    def recv_disconnect(self):
        pass







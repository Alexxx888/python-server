#add the project path to system variable


import os, sys

ab = os.path.abspath(r"E:/python server/python-server/OpenEdu/")

sys.path.append(ab)
sys.path.append("E:\\python server\\python-server\\OpenEdu\\OpenEdu")
print sys.path
os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
print os.environ['DJANGO_SETTINGS_MODULE']


from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from OpenEdu.OpenEdu.apps.main_site import models


sessionmodel_content_type = ContentType.objects.get_for_model(models.SessionModel)

message_ct=ContentType.objects.get_for_model(models.Messages)
notification_ct=ContentType.objects.get_for_model(models.Notifications)



edit_permission = Permission.objects.get(content_type=sessionmodel_content_type,codename='change_sessionmodel')
add_permission = Permission.objects.get(content_type=sessionmodel_content_type,codename='add_sessionmodel')
delete_permission = Permission.objects.get(content_type=sessionmodel_content_type,codename='delete_sessionmodel')

post_edit_permission =  Permission.objects.get(codename='change_notifications')
post_add_permission =  Permission.objects.get(codename='add_notifications')
post_delete_permission =  Permission.objects.get(codename='delete_notifications')


messages_edit_permission = Permission.objects.get(content_type=message_ct,codename='change_messages')
messages_add_permission = Permission.objects.get(content_type=message_ct,codename='add_messages')
messages_delete_permission = Permission.objects.get(content_type=message_ct,codename='delete_messages')

notifications_edit_permission = Permission.objects.get(content_type=notification_ct,codename='change_notifications')
notifications_add_permission = Permission.objects.get(content_type=notification_ct,codename='add_notifications')
notifications_delete_permission = Permission.objects.get(content_type=notification_ct,codename='delete_notifications')



userseen_edit_permission=  Permission.objects.get(codename='change_userseenmessages')
userseen_add_permission=  Permission.objects.get(codename='add_userseenmessages')
userseen_delete_permission=  Permission.objects.get(codename='delete_userseenmessages')

#edit_permission.save()
#add_permission.save()
#delete_permission.save()

if  not Group.objects.filter(name='Teachers').exists():
    teacher_group = Group(name='Teachers')
    teacher_group.save()

if  not Group.objects.filter(name='Students').exists():
    students_group = Group(name='Students')
    students_group.save()

try:
    teacher_group = Group.objects.get(name='Teachers')
    students_group = Group.objects.get(name='Students')
except:
    sys.stderr('Groups select query failed - prepare.py')
    sys.exit(1)



teacher_group.permissions.add(edit_permission)
teacher_group.permissions.add(add_permission)
teacher_group.permissions.add(delete_permission)

teacher_group.permissions.add(userseen_add_permission)
teacher_group.permissions.add(userseen_edit_permission)
teacher_group.permissions.add(userseen_delete_permission)

teacher_group.permissions.add(notifications_add_permission)
teacher_group.permissions.add(notifications_delete_permission)
teacher_group.permissions.add(notifications_edit_permission)

teacher_group.permissions.add(messages_add_permission)
teacher_group.permissions.add(messages_edit_permission)
teacher_group.permissions.add(messages_delete_permission)

teacher_group.permissions.add(post_add_permission)
teacher_group.permissions.add(post_delete_permission)
teacher_group.permissions.add(post_edit_permission)



students_group.permissions.add(edit_permission)
students_group.permissions.add(post_add_permission)
students_group.permissions.add(userseen_add_permission)
students_group.permissions.add(userseen_edit_permission)

# Create your views here.
from django.http import *
from django.views.generic import *
from django.core.urlresolvers import reverse
from django.views.generic.edit import *
from django.contrib.auth import *
from django.shortcuts import render
from django.template.response import TemplateResponse
import models
from django.template import RequestContext
from django.contrib.auth.decorators import *
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
import datetime
from django.db.models import *
import forms
from django.contrib import messages
from django.shortcuts import render_to_response
import json
from django.contrib.sessions.models import Session
from django.utils.encoding import force_str
from django.utils.timezone import utc
import sys
from django.db.models import Q



def get_active_user_courses(user):
    return models.SessionModel.objects.\
        annotate(number_of_participants=Count('participants')).\
        filter(participants=user)\
        .exclude(end_date__lt=datetime.datetime.utcnow().replace(tzinfo=utc))\
        .filter(ended=False)



#check for if a user belongs to group
def teacher_group_login_required(user):
    if user:
        return user.is_authenticated() and user.groups.filter(name="Teachers").count != 0
    return False

def user_is_in_course(user):
    if user.is_authenticated() and models.SessionModel.objects.filter(participants__id=user.id).exists():
        return True
    return False


class CreateRoomTest(TemplateView,ProcessFormView):
    template_name ='create_test.html'


    def post(self, request, *args, **kwargs):

        possbile_operations = set(['createtest', 'createquestion'])
        operation= [key for key in request.POST if key in possbile_operations ]

        if 'createtest' in request.POST:

            form = forms.CreateTestForm({'name':request.POST.get('name')})
            if form.is_valid():
                form.create(self.request,self.kwargs.get('course'))

        if 'createquestion' in request.POST and len(operation)==1:
            form = forms.CreateQuestionForm(request.POST)
            if form.is_valid():
                print 'yes'

        return HttpResponseRedirect(reverse('create_test_url',kwargs={'course':self.kwargs.get('course')}))


    @method_decorator(user_passes_test(teacher_group_login_required ))
    def dispatch(self, *args, **kwargs):
        return super(CreateRoomTest, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateRoomTest,self).get_context_data(**kwargs)
        context['form']=forms.CreateTestForm
        context['questions_form']=forms.CreateQuestionForm
        context['course_tests'] = models.Tests.objects.filter(course=models.SessionModel.objects.filter(unique_key=self.kwargs.get('course')).first())

        return context


class RoomView(TemplateView,ProcessFormView):
    template_name = 'rtc.html'


    @method_decorator(user_passes_test(user_is_in_course))
    def dispatch(self, *args, **kwargs):
        if models.SessionModel.objects.filter(unique_key=kwargs.get('course')).exists():
            return super(RoomView,self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):

        possbile_operations = set(['newcomment', 'checkforcomments'])
        operation= [key for key in request.POST if key in possbile_operations ]

        if 'newcomment' in request.POST and len(operation)==1:

            print request.POST
            form = forms.CreateCommentForm(request.POST)

            if form.is_valid():

                try:
                    new_comment = models.Comments(creator=request.user,
                                                  course=models.SessionModel.objects.get(unique_key=self.kwargs.get('course')),
                                                  text=request.POST.get('text'),
                                                  date_crated=datetime.datetime.utcnow().replace(tzinfo=utc)
                    )
                    new_comment.save()
                except Exception as e:
                    print e
                    sys.stderr('Could not create new comments RoomView in post')

        elif 'checkforcomments' in request.POST and len(operation)==1:

            course_comments = models.Comments.objects.filter(course=models.SessionModel.objects.get(unique_key=self.kwargs.get('course')))

            posts_difference= course_comments.count() - int(request.POST.get('total'))

            number_of_seen_courses = models.Comments.objects.filter(course=models.SessionModel.objects.get(unique_key=self.kwargs.get('course')),
                                                           seen_by__id=self.request.user.id).count()

            if posts_difference>0 and number_of_seen_courses<course_comments.count():
                return_data =[]
                number_of_new_comments = course_comments.count() - number_of_seen_courses
                print number_of_new_comments
                for comment in course_comments.order_by('-date_crated')[0:number_of_new_comments]:

                    comment.seen_by.add(self.request.user)
                    return_data.append({
                        'creator':comment.creator.username,
                        'date':'',
                        'message':comment.text
                    })
                #return HttpResponse(json.dumps(return_data),mimetype='application/json')
                return render_to_response('ajax.html',
                     {'posts':course_comments.order_by('-date_crated')[0:number_of_new_comments]},
                          context_instance=RequestContext(request),mimetype='application/json')

        return HttpResponse('')


    def get_context_data(self, **kwargs):
        context = super(RoomView,self).get_context_data(**kwargs)
        context['active_user_courses']=get_active_user_courses(self.request.user)
        context['chat_room_name']= self.kwargs.get('course')
        context['teacher'] = self.kwargs.get('teacher_name')
        context['loginform'] = forms.LoginForm

        context['comments']=models.Comments.objects.filter(course=models.SessionModel.objects.get(unique_key=self.kwargs.get('course')))\
                                                                                                    .order_by('-date_crated')

        for item in context['comments']:
            item.seen_by.add(self.request.user)

        return context






class UpdateSessionView(FormView):
    form_class = forms.UpdateSessionForm


    @method_decorator(user_passes_test(teacher_group_login_required ))
    def dispatch(self, *args, **kwargs):
        return super(UpdateSessionView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):

        form.mark_course(self.request)
        messages.add_message(self.request,messages.SUCCESS,"Successfully Reactivated the course")
        return HttpResponseRedirect(reverse('courses_url'))

    def form_invalid(self, form):
        print "invalid"
        return HttpResponseRedirect(reverse('courses_url'))


class TestRtcView (TemplateView):
    template_name = "rtc.html"

    def get_context_data(self, **kwargs):
        context = super(TestRtcView,self).get_context_data(**kwargs)
        context['loginform'] = forms.LoginForm
        return context



class CreateSessionView(FormView):
    form_class = forms.CreateSessionForm
    #template_name = "createsession.html"
    #success_url = '/test/createsession'



    @method_decorator(user_passes_test(teacher_group_login_required ))
    def dispatch(self, *args, **kwargs):
        return super(CreateSessionView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.save_session(self.request,user=self.request.user)
        messages.add_message(self.request,messages.SUCCESS,"Course added successfully")

        return HttpResponseRedirect(reverse('courses_url'))

    def form_invalid(self, form):
        #self.request.session['errors'] = form.errors
        print "tuka gledai "
        print form.errors
        messages.add_message(self.request,messages.ERROR,'OOPS')
        return HttpResponseRedirect(reverse('courses_url'))





class RegistrationView(FormView):
    form_class = forms.RegistrationForm
    template_name = 'registration.html'


    def form_valid(self, form):
        form.register(self.request)
        return HttpResponse("Registered successfully")


class LoginView(FormView):
    template_name = 'login.html'
    form_class = forms.LoginForm


    def form_valid(self, form):
        form_action_result = form.auth(form.cleaned_data, self.request)
        return form_action_result




class JoinSessionView(FormView):

    form_class = forms.JoinSessionForm
    template_name = 'index.html'


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(JoinSessionView,self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        action = form.join_session(self.request)
        messages.add_message(self.request,messages.SUCCESS,'Successfully joined the course')
        return HttpResponseRedirect(reverse('courses_url'))

    def form_invalid(self, form):
        messages.add_message(self.request,messages.ERROR,'OOPS something went wrong')
        return HttpResponseRedirect(reverse('courses_url'))





######### helper functions for check_for_notifications
### outsidee check_for_notifications because they will be compiled for every request to
def check_if_message_is_seen(message,user,notification):
        if models.UserSeenMessages.objects.filter(message_id=message.id,user=user).exists():
            return True

        else:
            return False



def mark_message_as_seen(courses_messages_map,user):

    if courses_messages_map:
        for message_pk in courses_messages_map:
            try:
                message_id = courses_messages_map[message_pk]

                ###if there is already userseenmessage item add new user that has seen it else create new item
                if models.UserSeenMessages.objects.filter(message=message_id).exists():
                    existing_item = models.UserSeenMessages.objects.get(message=message_id)
                    existing_item.user.add(user)
                else:
                    notification = models.Notifications.objects.get(messages=message_id)
                    message = models.Messages.objects.get(id=message_id)
                    new_seen_message = models.UserSeenMessages(message= message,seen=True,notification=notification)
                    new_seen_message.save()
                    new_seen_message.user.add( user)
            except Exception as ex:
                print ex
                print "error 1"
                pass
    return



def check_for_notifications(request):

    possbile_operations = set(['checknotifications', 'markmessagesseen','checknotifications2'])
    l= [key for key in request.POST if key in possbile_operations ]


    if request.user.is_authenticated:


        if 'checknotifications' in request.POST and len(l)==1:
            try:
                tz_diff = datetime.timedelta(days=0,hours=int(request.COOKIES['tz']))
            except:
                pass

            b=[]
            a= get_active_user_courses(request.user)

            if a:

                for item in a:
                    try:
                        notification = models.Notifications.objects.filter(course=item.pk).exclude(end_date__lt=datetime.datetime.utcnow().replace(tzinfo=utc)).last()
                        if notification is None:
                            continue
                        m = notification.messages.last()

                        b.append({

                            'course':item.unique_key,
                            'course_name':item.name,
                            'message_id':m.pk,
                            'message':m.messages,
                            'end_date':(notification.end_date-tz_diff).strftime("%d %b %H:%M:%S"),
                            'seen':check_if_message_is_seen(m,request.user,item)
                        })

                    except Exception as ex:
                        print ex
                        return HttpResponse(json.dumps({}),mimetype='application/json')


                return  HttpResponse(json.dumps(b),mimetype='application/json')

        if 'checknotifications2' in request.POST and  len(l)==1 :
            active_user_courses = get_active_user_courses(request.user)

            if active_user_courses:

                notifications = models.Notifications.objects.filter(course=active_user_courses)

                seen_messages =[]
                for item in notifications:
                    if not models.UserSeenMessages.objects.filter(notification=item,message=item.messages.last(),user=request.user).exists():
                        seen_messages.append(item)
                print seen_messages

                return render_to_response('ajax.html',
                     {'notifications':notifications, 'seen_messages':seen_messages},
                          context_instance=RequestContext(request),content_type ='application/json')


        if 'markmessagesseen' in request.POST and  len(l)==1:
            mark_message_as_seen(json.loads(request.POST.get('messages')),request.user)


    return HttpResponse(json.dumps({}),mimetype='application/json')




class CoursesView(ListView,ProcessFormView):

    model = models.SessionModel
    template_name = "courses.html"

    post_actions = set(['action','search'])

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CoursesView,self).dispatch(*args, **kwargs)


    def post(self, request, *args, **kwargs):

        post_operation = [item for item in self.post_actions if item in request.POST ]


        if "action" in request.POST:

            form = forms.CreateNotificationForm(request.POST)
            if form.is_valid():
                form.create_notification(self.request)

        if "search" in request.POST and len(post_operation)==1:

            return HttpResponse(reverse('courses_url',kwargs={'subject':request.POST.get('subject'),'course_name':request.POST.get('name')}))

        return HttpResponse('')

    def get_error_from_session(self,request):
        session = Session.objects.get(session_key=request.COOKIES['sessionid'])
        error_values=[]

        try:
            e = session.get_decoded()['errors']
            print e
            for error in e.keys():
                a=e.get(error)
                for item in a:
                    print force_str(item)
                #error_values.append(b)
            return error_values
        except KeyError:

            return None
        return None

    def get_context_data(self, **kwargs):
        errors = None
        errors= self.get_error_from_session(self.request)


        context = super(CoursesView, self).get_context_data(**kwargs)

        if errors is not None:
            context['errors'] = errors

        context['user_courses'] = models.SessionModel.objects.annotate(number_of_participants=Count('participants')) \
            .filter(participants=self.request.user) \
            .exclude(end_date__lt=datetime.datetime.utcnow().replace(tzinfo=utc))

        context['create_session_form'] = forms.CreateSessionForm
        context['notification_form'] = forms.CreateNotificationForm



######## searching for courses to display and search ######################
        if self.kwargs.get("subject"):
            search_filter = Q(subject=self.kwargs.get("subject"))
            if self.kwargs.get('course_name'):
                search_filter = search_filter & Q(name__icontains=self.kwargs.get("course_name"))
        else:
            search_filter=Q()


        context['object_list'] = models.SessionModel.objects.annotate(number_of_participants=Count('participants')) \
            .exclude(number_of_participants__gt=11) \
            .exclude(end_date__lt=datetime.datetime.utcnow().replace(tzinfo=utc)) \
            .exclude(participants=self.request.user) \
            .exclude(ended=True)\
            .filter(search_filter)

        context['active_user_courses'] = get_active_user_courses(self.request.user)
        context['is_Teachers'] = self.request.user.groups.filter(name='Teachers').exists()

        if self.request.user.groups.filter(name='Teachers').count() == 1:
            context['teacher_courses'] = models.SessionModel.objects.filter(creator_id=self.request.user.id)

        context['search_model'] = forms.SearchSession

######## searching for courses to display and search ######################


        return context




class LandingView(TemplateView):
    template_name = 'index.html'
    form_class = forms.LoginForm


    def get(self, request, *args, **kwargs):


        if 'next' in request.GET and not request.user.is_authenticated():
            messages.add_message(request,messages.WARNING,"You must first log in")

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


    def get_context_data(self, **kwargs):
        context = super(LandingView, self).get_context_data(**kwargs)
        context['loginform'] = self.form_class


        if self.request.user.is_authenticated():
            context['active_user_courses']=get_active_user_courses(self.request.user)

        return context


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('index_url'))

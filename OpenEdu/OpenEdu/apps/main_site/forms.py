import django.forms
from django.contrib.auth.models import User,Group, Permission
from django.contrib.auth import authenticate, login
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from models import *
import datetime
from django.utils.timezone import utc
import django.dispatch
from django.db.models import Count
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test

import sys



subject_list = (
            ('English','English'),
            ('German','German'),
            ('French','French')
        )

def teacher_group_login_required(user):
    if user:

        return user.is_authenticated() and user.groups.filter(name="Teachers").exists()

    return False

def create_utc_start_date(request):

    try:
            tz_diff = datetime.timedelta(hours=int(request.COOKIES['tz']))
    except RuntimeError:
        pass

    time_now = datetime.datetime.strptime(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M'),('%Y-%m-%d %H:%M'))
    time_now=time_now.replace(tzinfo=utc)
    return time_now,tz_diff



class CreateTestForm(forms.Form):

    name = forms.CharField(required=True,max_length=255)


    def create(self,request,course):
        course_model = SessionModel.objects.filter(unique_key=course).first()
        t = Tests(name=self.cleaned_data.get('name'),teacher=request.user,course=course_model)
        try:
            t.save()
        except:
            pass

class CreateQuestionForm(forms.ModelForm):

    question = forms.CharField(max_length=500)
    correct_answer = forms.ChoiceField(choices=(("a","a"),("b","b"),("c","c"),("d","d")))
    test = forms.ChoiceField()
    class Meta:
        model = Questions
        fields=['question','option_a','option_b','option_c','option_d']
        exclude = ['test']




class CreateCommentForm(forms.Form):

    unique_key = forms.IntegerField(widget=forms.HiddenInput,required=True)
    text= forms.CharField(required=True)


class CreateNotificationForm(forms.Form):
    unique_key = forms.IntegerField(widget=forms.HiddenInput)
    message  = forms.CharField(widget=forms.Textarea())
    end_date = forms.DateTimeField(widget=forms.TextInput(attrs={'id':'dtpicker'}),label='Course starts')



    @method_decorator(user_passes_test(teacher_group_login_required ))
    def create_notification(self,request):

        (start_date,tz_diff)=create_utc_start_date(request)
        end_date = datetime.datetime.strptime( self.cleaned_data['end_date'].strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M' )
        end_date=end_date.replace(tzinfo=utc)
        end_date+=tz_diff

        try:
            notif=Notifications.objects.get(creator=request.user,course=SessionModel.objects.get(unique_key=self.cleaned_data.get('unique_key')))
            notif.end_date = end_date
        except:

            notif = Notifications(creator=request.user,
                              course=SessionModel.objects.get(unique_key=self.cleaned_data.get('unique_key')),
                              start_date = start_date,
                              end_date = end_date )

        new_message = Messages(messages=self.cleaned_data.get('message'))
        new_message.save()

        notif.save()
        notif.messages.add(new_message)



        return


class UpdateSessionForm(forms.Form):

    unique_key = forms.IntegerField()
    operation = forms.CharField(max_length=255)
    end_date= forms.DateTimeField(required=False)

    @method_decorator(user_passes_test(teacher_group_login_required ))
    def mark_course(self,request):
        print self.cleaned_data

        course = SessionModel.objects.get(unique_key=self.cleaned_data.get('unique_key'))

        if self.cleaned_data.get('operation')=='activate':
            course.ended = False
        elif self.cleaned_data.get('operation')=='deactivate':
            course.ended = True

        elif self.cleaned_data.get('operation')=='dismissal':
            for user in course.participants.all():
                course.participants.remove(user)

        elif self.cleaned_data.get('operation')=='update':
            print 'yep'

            tz_diff = datetime.timedelta(hours=int(request.COOKIES['tz']))
            end_date = datetime.datetime.strptime( self.cleaned_data['end_date'].strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M' )
            end_date=end_date.replace(tzinfo=utc)
            end_date+=tz_diff
            course.end_date = end_date
            course.save()
        else:
            return


        course.save()


        return True




class JoinSessionForm(forms.Form):

    unique_key = forms.IntegerField()

    class Meta:
        model= SessionModel
        fields=()

    def join_session(self,request):
        print self.cleaned_data['unique_key']
        user = request.user
        session_model = SessionModel.objects.get(unique_key=self.cleaned_data.get('unique_key'))

        if session_model.participants.count()<11:
            session_model.participants.add(User.objects.get(email=user.email))

        return True

class CreateSessionForm(forms.ModelForm):

    class Meta:
        model= SessionModel
        fields=('name','subject','end_date')



        labels = {
            'name': 'Course Name',
            'end_date':'Course end date'
        }

        widgets={
            'end_date': forms.TextInput(attrs={'id':'dtpicker'}),
            'subject': forms.Select(choices=subject_list),
            'name':forms.Textarea(attrs={'style':'resize:none;height:30px'})
        }

    @method_decorator(user_passes_test(teacher_group_login_required ))
    def save_session(self,session,user=None):

        if user is not None:
            try:
                tz_diff = datetime.timedelta(hours=int(session.COOKIES['tz']))

            except RuntimeError:
                pass

            time_now = datetime.datetime.strptime(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M'),('%Y-%m-%d %H:%M'))
            time_now=time_now.replace(tzinfo=utc)

            end_date = datetime.datetime.strptime( self.cleaned_data['end_date'].strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M' )
            end_date=end_date.replace(tzinfo=utc)
            end_date+=tz_diff

            if time_now < end_date and user is not None:
                new_session = SessionModel(subject=self.cleaned_data['subject'],start_date=time_now,end_date=end_date,creator=user,ended=True,name=self.cleaned_data.get('name'))

                new_session.save()
                new_session.participants.add(user)





        return





class SearchSession(forms.Form):


    subjects = forms.ChoiceField(choices= subject_list)
    name = forms.CharField(max_length=255)





class RegistrationForm(forms.Form):

    account_type = forms.ChoiceField(choices=(('Teacher','Teacher'),('Student','Student')))
    username = forms.CharField(max_length=255,required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    first_name = forms.CharField(max_length=255,required=True)
    last_name = forms.CharField(max_length=255,required=True)
    email= forms.EmailField(required=True)

    widgets = {
        'account_type':forms.RadioSelect,
    }

    labels = {
        'username': "Username",
    }

    def register(self, request):
        try:
            User.objects.get(email=self.cleaned_data['email'])

        except User.DoesNotExist:

            user = User.objects.create_user(self.cleaned_data['username'], self.cleaned_data['email'],
                                            self.cleaned_data['password'])
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']

            try:

                if self.cleaned_data['account_type'] == 'Teacher':
                    teacher_gr = Group.objects.get(name='Teachers')
                    user.groups.add(teacher_gr)
                    user.save()

                if self.cleaned_data['account_type'] == 'Student':
                    student_gr = Group.objects.get(name='Students')
                    user.groups.add(student_gr)
                    user.save()

            except:
                sys.stderr('In forms.Registration register something bad happened')
                return HttpResponseRedirect(reverse("index_url"))

            login(request, authenticate(username=user.username, password=self.cleaned_data.get('password')))

        return HttpResponseRedirect(reverse("index_url"))


class LoginForm(forms.ModelForm):
    class Meta:
        model = OEUser
        fields = ['username', 'password']

        widgets = {
            'password': forms.PasswordInput,
        }

    def auth(self, form_data, request):
        user = authenticate(username=form_data['username'], password=form_data['password'])

        if user is not None:
            login(request, user)
            print user.user_permissions.all()
            print user.groups.all()
        """try:
			User.objects.get()
			print 'ok'
		except User.DoesNotExist:
			print 'fail'
			return """
        return HttpResponseRedirect(reverse("index_url"))


class LogoutForm(forms.ModelForm):
    class Meta:
        model = OEUser
    pass

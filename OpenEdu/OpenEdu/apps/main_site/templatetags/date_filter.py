from django import template
from datetime import datetime

register = template.Library()

@register.filter(expects_localtime=True)
@register.filter(name='businesshours')
def businesshours(value):
	
	time = datetime.now()
	if time is not None:
		return time
		
	return ''
		

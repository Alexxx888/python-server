from django.db import models
from django import forms
from django.contrib.auth.models import User
import random
import datetime

def randomize_key():

    while(True):
        rnd = random.randint(100000,1000000)
        if not SessionModel.objects.filter(unique_key=rnd).exists():
            return rnd



class SessionModel(models.Model):



    subject = models.CharField(max_length=255,blank=False)
    start_date = models.DateTimeField(blank=True)
    end_date = models.DateTimeField(blank=False)
    creator = models.ForeignKey(User)
    unique_key = models.PositiveIntegerField(default=randomize_key,unique=True)
    participants = models.ManyToManyField(User,related_name="students")
    name = models.CharField(max_length=255,blank=False)
    ended = models.BooleanField(default=True)




class Questions(models.Model):

    value = models.CharField(max_length=500)
    option_a = models.CharField(max_length=20)
    option_b = models.CharField(max_length=20)
    option_c = models.CharField(max_length=20)
    option_d = models.CharField(max_length=20)
    correct = models.CharField(max_length=20)



class Comments(models.Model):
    creator = models.ForeignKey(User)
    course = models.ForeignKey(SessionModel)
    text = models.TextField(blank=False)
    date_crated = models.DateTimeField()
    seen_by = models.ManyToManyField(User,related_name='seen_by')


class Messages(models.Model):
    messages=models.TextField()
    start_date = models.DateTimeField(default=datetime.datetime.utcnow())



class Notifications(models.Model):
    course=models.ForeignKey(SessionModel)
    creator = models.ForeignKey(User)
    messages = models.ManyToManyField(Messages,related_name='notification_messages')
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()


class UserSeenMessages(models.Model):
    notification = models.ForeignKey(Notifications)
    message = models.ForeignKey(Messages)
    user = models.ManyToManyField(User)
    seen = models.BooleanField()


class Tests(models.Model):

    name = models.CharField(max_length=255)
    teacher = models.ForeignKey(User)
    questions = models.ManyToManyField(Questions)
    course=models.ForeignKey(SessionModel)




class OEUser(models.Model):

    username = models.CharField( max_length = 255, blank = False)
    password = models.CharField( max_length = 255, blank = False  )
    first_name = models.CharField(max_length = 255, blank = False)
    last_name = models.CharField( max_length = 255, blank = False)
    email = models.EmailField( max_length = 255, blank = False)

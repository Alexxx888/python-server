from django.conf.urls import patterns, url

from views import *
from views import logout_view

urlpatterns = patterns('',
    url(r'^$', LandingView.as_view(), name='index_url'),
	url(r'^registration/$', RegistrationView.as_view(), name='registration_url'),
	url(r'^login/$', LoginView.as_view(), name= 'login_url'),
    url(r'^logout/$',logout_view,name='logout_url'),
    url(r'^createsession/$',CreateSessionView.as_view(),name='create_session_url'),
    url(r'^joinsession/$',JoinSessionView.as_view(), name='join_session_url'),
	url(r'^courses(?:/(?P<subject>[a-zA-Z]*))?(?:/(?P<course_name>[a-zA-Z]*))?/$'   ,CoursesView.as_view(),name='courses_url' ),
    url(r'tests/(?P<course>\d{6})/',CreateRoomTest.as_view(),name='create_test_url'),
    url(r'^rtc/$',TestRtcView.as_view(),name='rtc_url'),
    url(r'updatesession/$',UpdateSessionView.as_view(),name='update_session_url'),
    url(r'teacher/(?P<teacher_name>[a-z0-9_-]{3,15})/(?P<course>\d{6})/', RoomView.as_view(),name='rooms_url'),
    url(r'ajax/',check_for_notifications,name='notifications_url'),
)
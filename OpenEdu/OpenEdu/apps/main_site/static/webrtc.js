function OpenWebRtc() {


    var self = this;

    var url = window.location.host;
    var socket = io.connect(url + '/rtc', {port: 5462}, function (e) {
        console.log("connected");
    });

    var chat_socket = io.connect(url + '/chat', {port: 5462}, function (data) {

    });

    $(window).unload(function () {
        socket.disconnect();
        chat_socket.disconnect();
    });

     OpenWebRtc.prototype.initChat = function (room) {
        room_name = room;



        chat_socket.emit('chat_room_created', {
            room: room,
            'sender':user
        })
    }


    var room_name=window.location.href.split("/")[6];
    if (!room_name){
        self.initChat('1');
    }



    OpenWebRtc.prototype.sendMessage = function (params) {


        if (!room_name && params['room']) {
            this.initChat(params['room']);
        }

        //console.log("tesint " + params['data']+ " room= "+ room_name);


       if(params['data'].length>1){
            chat_socket.emit('sendMessage', {

                'data': params['data'] + "\n" + "\n",
                'room': room_name,
                'sender': user
            })
       }

        var input = $("#recieved_messages");
        input.val('');
        input.focus();

    }


    var sound;
    var height_count=0;
    chat_socket.on('receivedMessage', function (data) {

        if (data.sender !== user) {
            console.log("sender =" + data.sender);
            sound.get(0).play();
        }
        var chat = $('#chat_messages_container');
        console.log(data);
        if (data.username){
            var username = $("<font color='#0099FF'></font>").text(data['username'])
            var text = $("<font></font>").text(" : "+data.data);
            var paragraph = $("<p><br></p>")

                username.appendTo(paragraph);
                text.appendTo(paragraph);
                paragraph.appendTo(chat);
                chat.scrollTop(height_count+paragraph.height());
                height_count = height_count+paragraph.height();
                console.log(height_count);

        }





        //chat.scrollTop(chat.get(0).scrollHeight);


    });



    var constraints = {
        "audio": true,
        "video": {
            "mandatory": {
                "minWidth": "320",
                "maxWidth": "1920",
                "minHeight": "240",
                "maxHeight": "1080",
                "minFrameRate": "30"
            },
            "optional": []
        }
    };

    var pc_constraints = {
        'optional': [
            {'DtlsSrtpKeyAgreement': true},
            {'RtpDataChannels': false}
        ]};

    var servers = navigator.userAgent.indexOf('Firefox') === 1 ?

    {iceServers: [
        {"url": "stun:stun.l.google.com:19302"},
        {url: 'stun2.l.google.com:19302'},
        {url: 'stun1.l.google.com:19302'},
        {urls: 'stun:23.21.150.121'},
        {url: 'stun.ideasip.com'}

    ]}
        :

    { iceServers: [
        {url: "stun:23.21.150.121"},
        {url: "stun:stun.l.google.com:19302"},
        {url: "turn:numb.viagenie.ca", credential: "webrtcdemo", username: "louis%40mozilla.com"}
    ]};


    var user = makeid();
    var otherUser = null;
    var localStream = null;
    var peer = null;
    var callholder = null ///// holds the calls

    self.peerList = {};
    self.trackList = {};

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        user = text;
        socket.emit('registerUser', {user:text,room:room_name});
        //socket.emit('getAllUsers',{'room':room_name});
        socket.emit('getAllUsers',{'room':room_name});

        return text;
    }


    OpenWebRtc.prototype.getPeer = function () {
        return peer;
    }

    OpenWebRtc.prototype.getStream = function () {
        return localStream;
    }


    function renegotiation(peer) {
        ///advash track kum streama,disable koito ne iskash,emit streama/tracka, drugiq go poluchava i si adva tracka ot nego
        /*console.log('renegotiation')
         for(i in peer){
         console.log(i+" "+peer[i])
         }
        socket.emit("renegotiation", {
            'caller': user,
            'target': peer.otherUser,


        })*/

    }


    OpenWebRtc.prototype.connectToUser = function (data) {

        if (callholder === null) {
            window.alert("You will be assigned to hold tha call");
            callholder = true;
        }


        if (peer && otherUser) {
            console.log('da' + "otherUser = " + data);
            saveOldPeerPairCreateNew(data);
        }

        otherUser = data;

        self.setUp();

        sendCallRequestToOtherUser();
    }


    function saveOldPeerPairCreateNew(data) {

        peer = createPeerConnection();

    }


    function addStreamAndCreateOffer() {


        peer.createOffer(function (offerSDP) {
            offerSDP.sdp = offerSDP.sdp.replace(/b=AS([^\r\n]+\r\n)/g, '');
            offerSDP.sdp = offerSDP.sdp.replace(/a=mid:audio\r\n/g, 'a=mid:audio\r\nb=AS:50\r\n');
            offerSDP.sdp = offerSDP.sdp.replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:256\r\n');
            peer.setLocalDescription(offerSDP);
            sendLocalDspOrIce({'caller': user, 'targetUser': otherUser, 'SDP': offerSDP, 'negotiation': negotiation});



        });

    };


    function removeDescriptionsAndIce() {
        peer.remoteDescription = null;
        peer.localDescription = null;
    }


//////////////////////////////////////////////////////////////////////////////////////////////////


    socket.on('returnAllUsers', function (userList) {
        console.log(userList);

        var ulItem = $('#userList');
        ulItem.empty();
        $.each(userList, function (k, v) {

            var newLi = $('<li ></li>');
            var icon = $('<span class="glyphicon glyphicon-remove-circle" style="color: red !important;font-size:20px;"></span>');
            icon.appendTo(newLi);

            var en = '\'' + k + '\'';
            newLi.append('<a href="#" onclick="a.connectToUser(' + en + ')">' + v + '</a>');
            newLi.append(icon);

            newLi.appendTo(ulItem);
            /*var liValue = document.createTextNode(entry);
             newLi.appendChild(liValue);
             ulItem.appendChild(newLi);*/
        });
    });


    socket.on("renegotiation", function (data) {
        if (data.caller !== user && data.target === user && data) {
            removeDescriptionsAndIce();
            //peer.updateIce()
            console.log("renegotiation");
            for (i in peer) {
                console.log(i + " " + peer[i])
            }

            socket.emit('removeDone', {
                'caller': user,
                'target': callholder,
            })

        }
    });

    socket.on("removeDone", function (data) {
        currentPeer = peer;
        peer = self.peerList[data.caller]
        //peer.updateIce();
        addStreamAndCreateOffer();

    })


    function sendCallRequestToOtherUser() {
        socket.emit('call',

            {
                caller: user,
                targetUser: otherUser,
                message: 'user = ' + user + 'is requesting call'
            }
        );
    }


    //////////////////callee receives the socke.emit('call'); ////////////////
    socket.on('receivedCall', function popCallMessage(data) {
        if (data.caller !== user && data.targetUser === user && data.message) {
            var r = window.confirm(data.message);

            socket.emit('callAction', {
                caller: user,
                targetUser: data.caller,
                accepted: r
            });


            //////////// initiate local stream if callee accepted the offer
            if (r) {
                otherUser = data.caller;
                //callholder = data.caller; ///// set so people that are already connected cant choose audio source
                self.setUp();
            }
        }
    });


    socket.on('gotLocalDSP', function (data) {
        //console.log('got local dsp');

        if (data.targetUser === user && data.caller !== user) {

            if (!peer.remoteDescription && !peer.localDescription) {
                //console.log('got to lvl 1');
                ///////////////////////// set callee/remotepeer - remote description and create answer
                if (data.targetUser === user && data.SDP) {

                    //peer.addStream(localStream);
                    var remoteDescription = createSessionDescription(data.SDP);
                    peer.setRemoteDescription(remoteDescription);

                    peer.createAnswer(function (answerSDP) {
                        peer.setLocalDescription(answerSDP);
                        //console.log(localIceCandidate+" dda " + answerSDP+ data.caller+data.target);
                        sendLocalDspOrIce({'caller': user, 'targetUser': otherUser, 'SDP': answerSDP});


                    }, onfailure, sdpConstraints);


                }

            } else if (data.candidate && data.caller !== user && data.targetUser === user) {

                ///received ICECandidate add
                var otherCandidate = data.candidate.candidate;
                var sdpindex = data.candidate.sdpMLineIndex;

                if (!otherCandidate || !sdpindex) {
                 //   console.log('fail' + 'caller= ' + data.caller + ' ' + 'targetUser= ' + data.targetUser);
                }

                peer.addIceCandidate(createIceCandidate(sdpindex, otherCandidate));


            } else if (!peer.remoteDescription && peer.localDescription) {
                //////sets caller/local peer remote description
                var remoteDescription = createSessionDescription(data.SDP);
                //console.log("test ==== " + remoteDescription);
                peer.setRemoteDescription(remoteDescription);

            } else {
                //console.log('already in call ' + data.SDP + data.candidate + data.caller + data.targetUser)
            }
            ;
        }
    });


    socket.on('user_is_ready', function (data) {

        var name = data.username;

        var s = $('li:contains("' + name + '")')
        var span = s.filter(function () {
            return $(this).text() == name;
        })

        span.find('span').attr('class', 'glyphicon glyphicon-ok').css({
            'color': 'black'
        });

        $('#start').click();

    });


//////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////Functions for mozzila to chrome support/////////////////////
    function createPeerConnection(params) {
        if (navigator.userAgent.indexOf('Firefox') !== -1) {

            var peer = new mozRTCPeerConnection(servers, pc_constraints);

        } else {
            var peer = new webkitRTCPeerConnection(servers, pc_constraints);
        }

        if (peer) {
            peer.onicecandidate = iceCandidate;
            peer.onaddstream = startRemote;
            peer.onRemoveStream = streamRemoved;
            peer.oniceconnectionstatechange  = checkConnection;
            peer.onnegotiationneeded = negotiation;
            return peer;
        }
    }

    function createSessionDescription(SDP) {


        if (navigator.userAgent.indexOf('Firefox') !== -1) {
            return new mozRTCSessionDescription(SDP);


        } else {
            return new RTCSessionDescription(SDP);

        }

    }

    function createIceCandidate(sdpindex, otherCandidate) {
        if (navigator.userAgent.indexOf('Firefox') !== -1) {
            return  new mozRTCIceCandidate({  sdpMLineIndex: sdpindex,
                candidate: otherCandidate});


        } else {
            return  new RTCIceCandidate({  sdpMLineIndex: sdpindex,
                candidate: otherCandidate});
        }
    }

///////////////////////Fucntions for mozilla to chrome support//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////pre-set local stream view////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    OpenWebRtc.prototype.setUp = function () {


        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        navigator.getUserMedia(constraints, getStream, errorGetStream);

    }

    function localStreamReadyNotify(params) {
        socket.emit('userInitiatedLocalStream',
            {
                'name': user
            });

    }

    function getStream(stream) {

        if (!peer)
            peer = createPeerConnection();

        if (!localStream) {
            localStream = stream;
        }

        peer.addStream(localStream);
        if (callholder !== true) {

            console.log('added local stream');

        } else if (callholder === true) {
            peer.otherUser = otherUser;
            peer.user = user;
            self.peerList[otherUser] = peer;
        }

        var vid = document.querySelector("#local");
        vid.src = window.URL.createObjectURL(stream);
        vid.muted = true;
        vid.play(localStreamReadyNotify());


    }

    function checkConnection(event) {


            if(event.srcElement.iceConnectionState==='disconnected'){
                self.peerList[event.srcElement.otherUser].close();
                delete self.peerList[event.srcElement.otherUser];
                $("#"+event.srcElement.otherUser).remove();
            }



    }

    function streamRemoved(event) {
        console.log("stream removed: " + event);
    }


    function iceCandidate(event) {
        sendLocalDspOrIce({'caller': user, 'targetUser': otherUser, 'SDP': peer.localDescription, 'candidate': event.candidate});
    }


    function startRemote(event) {
        console.log('started Remote stream mby' + otherUser);

        var remoteDiv = document.querySelector('#remoteDiv');
        //var baseVideo = $("#base_element").clone();

        var newVideo = $('<video></video>').attr({'id': otherUser, 'class': 'video-js vjs-default-skin'}).css({width: '300px', height: '300px'}).appendTo($("#remoteDiv"));
        newVideo.attr("controls", true);
        var source = $('<source src="" type="video/ogg">');
        source.attr({'src': window.URL.createObjectURL(event.stream)});
        source.appendTo(newVideo);
        //newVideo.attr({'src':window.URL.createObjectURL(event.stream)});


        ////// if remotepeer/callee adds a stream and you are the call holder get his audio
        if (callholder === true) {
            var track = event.stream.getAudioTracks()[0];
            self.trackList[otherUser] = track;
            //track.enabled=false;
            //localStream.addTrack(track);
        }

        //console.log(localStream.getAudioTracks()[1]);
        newVideo.get(0).play();

    };

    function negotiation(event) {

        if (callholder === true) {
            renegotiation(event.srcElement);
            console.log(event.srcElement)
            //addStreamAndCreateOffer(true);
        }
    }

    function errorGetStream(error) {
        console.log("get stream error", error);
    }


/////////////////////////////////pre-set local stream view




    window.onload = function () {


        sound = $("#notification");
        sound.on("ended", function () {
            sound.get(0).pause();
            sound.currentTime = 0;
        });

        $("#recieved_messages").keydown(function (e) {

            if (e.which == 13) {
                $('#send').trigger("onclick");
            }

        })


        var regButton = document.getElementById('getUser');
        var startButton = document.querySelector('#start');

        startButton.disabled = true;
/*
        var startHolderStream = document.getElementById('send_callholder_stream')
        startHolderStream.onclick = function () {
            //console.log(localStream.toString() +" "+ peerList.toString());
            for (i in self.peerList) {
                self.peerList[i].addStream(localStream);
                self.peerList[i].localDescription = null;
                self.peerList[i].remoteDescription = null;
            }
        }*/

        regButton.onclick = function () {

            otherUser = document.querySelector('#user').value;

            sendCallRequestToOtherUser();


        };


        socket.on('callAction', function (data) {
            if (data.accepted === true) {
                startButton.disabled = false;

            }
        });


        //var span = document.querySelector('#genUser');
        //span.innerHTML = user;


        /////// init stream,send request, add caller stream and create offer,

        //////////////////////////////////Initiate call///////////////////////////////////////////////////////////////////////////
        startButton.onclick = addStreamAndCreateOffer;


    }
    /*
     socket.on('callerCandidates', function(data){

     if( data.caller!==user  && data.targetUser=== user && data.candidate){
     var otherCandidate= data.candidate.candidate;
     var sdpindex = data.candidate.sdpMLineIndex;
     if(!otherCandidate ||  !sdpindex){
     //console.log('fail'+ 'caller= '+data.caller + ' '+'targetUser= ' +data.targetUser);
     }

     if(!peer.remoteDescription)
     console.log('asd');

     peer.addIceCandidate( new RTCIceCandidate( {  sdpMLineIndex: sdpindex,
     candidate: otherCandidate})
     );

     }
     })*/


    /*
     //callee returned SDP
     socket.on('setCallerRemoteSDP',function(data){
     console.log(data.targetUser +" "+data.caller);
     if(data.targetUser===user && data.answerSDP){

     var remoteDescription = new RTCSessionDescription(data.answerSDP);
     peer.setRemoteDescription(remoteDescription);
     peerQueue[data.caller] = peer;


     }
     })*/

    ///////////////////Initiate call////////////////////////////////////////////////////////////////////////////


    function sendLocalDspOrIce(params) {
        //user,caller,{'candidate':candidate,'answerDSP':answerDSP}
        socket.emit('sendLocalDspOrIce',
            {
                caller: params['caller'],
                targetUser: params['targetUser'],
                SDP: params['SDP'],
                candidate: params['candidate']
            });
    }


/////////////////////////////////////ANSWERING --- END ////////////////////////////////////////////

    var sdpConstraints = {
        optional: [],
        mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        }
    };

    function onfailure(error) {
        console.log(error);
    }


    /////////////////////////////////SET OPUS//////////////////////////////////////////////////////////////////////
    // Set Opus as the default audio codec if it's present.
    function preferOpus(sdp) {
        var sdpLines = sdp.split('\r\n');
        var mLineIndex;
        // Search for m line.
        for (var i = 0; i < sdpLines.length; i++) {
            if (sdpLines[i].search('m=audio') !== -1) {
                mLineIndex = i;
                break;
            }
        }
        if (mLineIndex === null) {
            return sdp;
        }

        // If Opus is available, set it as the default in m line.
        for (i = 0; i < sdpLines.length; i++) {
            if (sdpLines[i].search('opus/48000') !== -1) {
                var opusPayload = extractSdp(sdpLines[i], /:(\d+) opus\/48000/i);
                if (opusPayload) {
                    sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], opusPayload);
                }
                break;
            }
        }

        // Remove CN in m line and sdp.
        sdpLines = removeCN(sdpLines, mLineIndex);

        sdp = sdpLines.join('\r\n');
        return sdp;
    }

    function extractSdp(sdpLine, pattern) {
        var result = sdpLine.match(pattern);
        return result && result.length === 2 ? result[1] : null;
    }

    // Set the selected codec to the first in m line.
    function setDefaultCodec(mLine, payload) {
        var elements = mLine.split(' ');
        var newLine = [];
        var index = 0;
        for (var i = 0; i < elements.length; i++) {
            if (index === 3) { // Format of media starts from the fourth.
                newLine[index++] = payload; // Put target payload to the first.
            }
            if (elements[i] !== payload) {
                newLine[index++] = elements[i];
            }
        }
        return newLine.join(' ');
    }

    // Strip CN from sdp before CN constraints is ready.
    function removeCN(sdpLines, mLineIndex) {
        var mLineElements = sdpLines[mLineIndex].split(' ');
        // Scan from end for the convenience of removing an item.
        for (var i = sdpLines.length - 1; i >= 0; i--) {
            var payload = extractSdp(sdpLines[i], /a=rtpmap:(\d+) CN\/\d+/i);
            if (payload) {
                var cnPos = mLineElements.indexOf(payload);
                if (cnPos !== -1) {
                    // Remove CN payload from m line.
                    mLineElements.splice(cnPos, 1);
                }
                // Remove CN line in sdp
                sdpLines.splice(i, 1);
            }
        }

        sdpLines[mLineIndex] = mLineElements.join(' ');
        return sdpLines;
    }


}
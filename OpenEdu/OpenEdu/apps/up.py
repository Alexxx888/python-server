import os, sys
print sys.path
a = os.path.abspath(r"C:\Users\Dimension Master\Desktop\python server\OpenEdu\OpenEdu")
sys.path.append(a)
print sys.path

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from south.migration import post_migrate

def update_permissions_after_migration(app,**kwargs):
    """
    Update app permission just after every migration.
    This is based on app django_extensions update_permissions management command.
    """
    import OpenEdu.OpenEdu.settings as settings
    from django.db.models import get_app, get_models
    from django.contrib.auth.management import create_permissions

    create_permissions(get_app(app), get_models(), 2 if settings.DEBUG else 0)

post_migrate.connect(update_permissions_after_migration('main_site'))
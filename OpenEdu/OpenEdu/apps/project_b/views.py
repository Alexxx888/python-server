from django.shortcuts import render
from django.views.generic import *
import forms
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template.response import TemplateResponse
from django.utils.translation import ugettext
from django.utils  import translation
# Create your views here.

class ContactForm(FormView):
    template_name = 'project_b/index.html'
    form_class = forms.ContactForm
    success_url = '/'
    some_string = ugettext("ad")
    def form_valid(self, form):
        check = form.save_contact()

        if check:

            return self.render_to_response(self.get_context_data(success=True))
        return HttpResponseRedirect(reverse('index_url'))

    def get_context_data(self, **kwargs):
        context = super(ContactForm,self).get_context_data(**kwargs)
        context['form'] = self.get_form_class()
        if kwargs.get('success') is not None:
            context['success_contact_registration'] =True

        return context


class IndexView(TemplateView):
  template_name = 'project_b/index.html'

  def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['form'] = forms.ContactForm
        return context
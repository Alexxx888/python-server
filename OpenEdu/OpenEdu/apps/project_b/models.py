from django.db import models
# Create your models here.
from django.utils.translation import ugettext_lazy
class ContactModel(models.Model):
  first_name = models.CharField(max_length = 64,blank = False)
  last_name = models.CharField(max_length = 64,blank = False)
  phone_number = models.CharField(max_length=65, blank = False, unique=True, error_messages={'unique':"The phone number is already registered"}, )
  email = models.EmailField(blank = False, unique=True,error_messages={'unique':"This email has already been registered."})
  
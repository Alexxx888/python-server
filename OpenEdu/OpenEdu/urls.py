from django.conf.urls import patterns, include, url

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'OpenEdu.views.home', name='home'),
    # url(r'^OpenEdu/', include('OpenEdu.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
	url(r'^test/', include('OpenEdu.apps.main_site.urls')),
    #url(r'^project/', include('OpenEdu.apps.project_b.urls')),
	
	
) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT) #+i18n_patterns('',url(r'^project/', include('OpenEdu.apps.project_b.urls')),)
